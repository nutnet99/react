export default [
    {
      id: "8809057515664",
      name: "ROJUKISS PERFECT PORELESS 5X MASK 25ML.",
      description: "โรจูคิสเพอร์เฟคพอร์เลส มาร์ค 25มล",
      price: 69,
      promotion : "1 get 1",
      image: "https://publish-p33706-e156581.adobeaemcloud.com/content/dam/aem-cplotusonlinecommerce-project/th/images/magento/catalog/product/new/86368/75450976.jpg/jcr:content/renditions/medium.png"
    },
    {
      id: "8850006303269",
      name: "COLGATE PLAX SALT HERBAL MOUTHWASH 500 M",
      description:
        "คอลเกตพลักซ์น้ำยาบ้วนปากซอลท์เฮอเบิ้ล500",
      price: 145,
      promotion : "1 get 1",
      image: "https://publish-p33706-e156581.adobeaemcloud.com/content/dam/aem-cplotusonlinecommerce-project/th/images/magento/catalog/product/new/86368/18040187.jpg/jcr:content/renditions/medium.png"
    },
    {
      id: "8850002024564",
      name: "SHOKUBUTSU SHOWER CREAM(R) CHINESE MILK",
      description:
        "โชกุบุสซึครีมอาบน้ำรีฟิล ผิวชุมชื่น500มล",
      price: 98,
      promotion : "1 get 1",
      image: "https://publish-p33706-e156581.adobeaemcloud.com/content/dam/aem-cplotusonlinecommerce-project/th/images/magento/catalog/product/new/86368/25430505.jpg/jcr:content/renditions/medium.png"
    },
    {
      id: "8851989060163",
      name: "D-NEE DIAPER WASH 600 ML. BLUE (REFILL)",
      description: "ดีนี่ซักผ้าเด็กถุง ฟ้า 600 มล.",
      price: 96,
      promotion : "1 get 1",
      image: "https://publish-p33706-e156581.adobeaemcloud.com/content/dam/aem-cplotusonlinecommerce-project/th/images/magento/catalog/product/new/73916/11332123.jpg/jcr:content/renditions/medium.png"
    }
  ];
  