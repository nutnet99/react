import React from "react"
import {useParams} from "react-router-dom"
import productsData from "./productsData"

function ProductDetail() {
    const {productId} = useParams()
    const thisProduct = productsData.find(prod => prod.id === productId)
    
    return (
        <div>
            <h1>{thisProduct.name}</h1>
            <p>Price: ฿{thisProduct.price}</p>
            <p>{thisProduct.description}</p>
            <h2>Promotion: {thisProduct.promotion}</h2>
            <div> <img src={thisProduct.image} alt="new" class="center"/> </div> 

        </div>   
    )
}

export default ProductDetail
